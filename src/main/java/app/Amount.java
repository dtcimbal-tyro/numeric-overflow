package app;

import java.math.BigInteger;

/**
 * Amount is a positive number
 */
public class Amount implements Comparable<Amount> {

    private static BigInteger INT_MIN_VALUE = BigInteger.valueOf(0);

    private static BigInteger INT_MAX_VALUE = BigInteger.valueOf(Integer.MAX_VALUE);

    public static Amount from(BigInteger amount) {
        if (amount.compareTo(INT_MIN_VALUE) < 0 || amount.compareTo(INT_MAX_VALUE) > 0) {
            throw new IllegalArgumentException("Illegal input. Provided value is outside Integer boundaries");
        }
        return new Amount(amount);
    }

    @Override
    public int compareTo(Amount other) {
        return this.value.compareTo(other.value);
    }

    public Amount plus(Amount other) {
        return Amount.from(this.value.add(other.value));
    }

    public Amount minus(Amount other) {
        return Amount.from(this.value.subtract(other.value));
    }

    private final BigInteger value;

    private Amount(BigInteger value) {
        this.value = value;
    }

    BigInteger getValue() {
        return value;
    }
}
