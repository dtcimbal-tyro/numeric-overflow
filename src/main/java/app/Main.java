package app;

import java.math.BigInteger;

public class Main {

    private Amount threshold = Amount.from(BigInteger.valueOf(1000));

    public static void main(String[] args) {

        Main app = new Main();

        Amount amount = Amount.from(BigInteger.valueOf(999));
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = Amount.from(BigInteger.valueOf(2000));
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(Amount amount) {
        return amount.compareTo(threshold) >= 0;
    }

}
